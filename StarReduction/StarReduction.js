// ****************************************************************************
// pjsr/StarReduction.js - Released 24/01/2015
// Version: 1.02
// ****************************************************************************
// Copyright (c) 2014-2015, Dave Watson. All Rights Reserved.
// Website: http://www.qdigital-astro.com
// Email:   dave@qdigital-imaging.com
//
// Redistribution and use in both source and binary forms, with or without
// modification, is permitted provided that the following conditions are met:
//
// 1. All redistributions of source code must retain the above copyright
//    notice, this list of conditions and the following disclaimer.
//
// 2. All redistributions in binary form must reproduce the above copyright
//    notice, this list of conditions and the following disclaimer in the
//    documentation and/or other materials provided with the distribution.
//
// 3. All products derived from this software, in any form whatsoever, must
//    reproduce the following acknowledgment in the end-user documentation
//    and/or other materials provided with the product.
//
// 4. This product is based on software from the PixInsight project, developed
//    by Pleiades Astrophoto and its contributors (http://pixinsight.com/).
//
// 	  This file executes within the PixInsight JavaScript Runtime (PJSR).
// 	  PJSR is an ECMA-262 compliant framework for development of scripts on the
//    PixInsight platform.
//    Copyright (c) 2003-2014, Pleiades Astrophoto S.L. All Rights Reserved.
//
// THIS SOFTWARE IS PROVIDED BY DAVE WATSON "AS IS" AND ANY EXPRESS OR IMPLIED 
// WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF 
// MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
// IN NO EVENT SHALL DAVE WATSON BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
// SPECIAL, EXEMPLARY OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, 
// BUSINESS INTERRUPTION; PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; AND LOSS
// OF USE, DATA OR PROFITS) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER
// IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
// ****************************************************************************

/*
 * StarReduction.js
 *
 * A utility script for the reduction of star size and optional sharpening of the stars after reduction. The script is designed to processes non-linear monochrome
 * images that have have been cropped, processed with Dynamic Background Extraction (DBE) and stretched with HT. 
 *
 * This script incorporates modified FWHM measurement code from Mike Schuster's FWHMEccentricity script.
 *
 */
 
#define __DEBUG     false

#include <pjsr/Sizer.jsh>
#include <pjsr/FrameStyle.jsh>
#include <pjsr/TextAlign.jsh>
#include <pjsr/StdButton.jsh>
#include <pjsr/StdIcon.jsh>
#include <pjsr/UndoFlag.jsh>
#include <pjsr/ColorSpace.jsh>
#include <pjsr/NumericControl.jsh>
#include <pjsr/SampleType.jsh>
#include <pjsr/StarDetector.jsh>

#feature-id    Utilities > StarReduction

#feature-info  A script for star size reduction.<br/>\
   <br/>\
   A utility script for the reduction of star size and optional sharpenening of the stars after reduction.<br/>\
   The script is designed to processes linear monochrome images that have have been cropped,<br/>\
   processed with Dynamic Background Extraction (DBE), and initially stretch with Histogram Transform.<br/>\ 
   <br/>\
   Copyright &copy; 2014-2015 Dave Watson

#feature-icon  StarReduction.xpm

#define VERSION "1.02"
#define TITLE "StarReduction v" + VERSION

/*
 * Default Parameters
 */
#define DEFAULT_SHOW_HELP		true		// true = show help text, false = don't show
#define DEFAULT_WARNING_RGB		false		// true = warn if RGB image, false = no warning
#define VERBOSE					true		// Verbose mode if true

var window = ImageWindow.activeWindow;
var SourceView;			
var TargetView;

// ******************************************
// * Data									*
// ******************************************
function StarReductionData() {
	this.TargetValid = false;
	
	// Get access to the active image window
	if (!window.isNull) {this.targetView = window.currentView;}
	this.keywords = new Array();
	
	// Set default parameters
	this.minSMThreshold = 0.0;
	this.maxSMThreshold = 1.0;
	this.defSMThreshold = 0.15;
	this.SMThreshold = this.defSMThreshold;
	
	this.minSMScale = 4;
	this.maxSMScale = 8;
	this.defSMScale = 6;
	this.SMScale = this.defSMScale;
	
	this.minSMSmoothness = 0;
	this.maxSMSmoothness = 10;
	this.defSMSmoothness = 4;
	this.SMSmoothness = this.defSMSmoothness;
	
	this.defOptSharpen = false;
	this.optSharpen = this.defOptSharpen;
	this.defOptMeasure = false;
	this.optMeasure = this.defOptMeasure;
	this.defOptShowMask = false;
	this.optShowMask = this.defOptShowMask;
	
	this.minLogStarDetectionSensitivity = -3.0;
   	this.maxLogStarDetectionSensitivity = 3.0;
   	this.defLogStarDetectionSensitivity = -1.0;
   	this.logStarDetectionSensitivity = this.defLogStarDetectionSensitivity;

    this.minUpperLimit = 0.0;
    this.maxUpperLimit = 1.0;
    this.defUpperLimit = 1.0;
    this.upperLimit = this.defUpperLimit;
	
	// Set default variables
    this.resolution = 65535.0;
    this.starSupport = 0;
    this.starProfiles = new Array();

   	this.defModelFunctionIndex = 4;
  	this.modelFunctionIndex = this.defModelFunctionIndex;
    this.modelFunctionNormalizations = new Array(
      2.0 * Math.sqrt(2.0 * Math.log(2.0)), 			// Gaussian
      2.0 * Math.sqrt(Math.pow(2.0, 1.0 / 10.0) - 1.0), // Moffat 10
      2.0 * Math.sqrt(Math.pow(2.0, 1.0 / 8.0) - 1.0), 	// Moffat 8
      2.0 * Math.sqrt(Math.pow(2.0, 1.0 / 6.0) - 1.0), 	// Moffat 6
      2.0 * Math.sqrt(Math.pow(2.0, 1.0 / 4.0) - 1.0), 	// Moffat 4
      2.0 * Math.sqrt(Math.pow(2.0, 1.0 / 2.5) - 1.0), 	// Moffat 2.5
      2.0 * Math.sqrt(Math.pow(2.0, 1.0 / 1.5) - 1.0), 	// Moffat 1.5
      2.0 * Math.sqrt(Math.pow(2.0, 1.0 / 1.0) - 1.0) 	// Lorentzian
   );
   
   this.modelFunctionLabels = new Array(
      "Gaussian",
      "Moffat10",
      "Moffat8",
      "Moffat6",
      "Moffat4",
      "Moffat2.5",
      "Moffat1.5",
      "Lorentzian"
   );
}

var data = new StarReductionData();

// ******************************************
// * Process								*
// ******************************************
function StarReductionProcess(data) {
	console.show();
	OpenViews();	
	var TargetCloneView = new ImageWindow(SourceView.image.width,
								  		  SourceView.image.height,
								  		  SourceView.image.numberOfChannels,
								  		  SourceView.window.bitsPerSample,
								  		  SourceView.window.isFloatSample,
								  		  SourceView.image.colorSpace != ColorSpace_Gray,
								  		  "TargetCloneView" );
	TargetCloneView.mainView.beginProcess(UndoFlag_NoSwapFile);
	TargetCloneView.mainView.image.apply( SourceView.image );
	TargetCloneView.mainView.endProcess();

	var MaskView = new ImageWindow( 	  SourceView.image.width,
								  		  SourceView.image.height,
								 		  SourceView.image.numberOfChannels,
								  		  SourceView.window.bitsPerSample,
								  		  SourceView.window.isFloatSample,
								  		  SourceView.image.colorSpace != ColorSpace_Gray,
								  		  "MaskView" );
	MaskView.mainView.beginProcess(UndoFlag_NoSwapFile);
	MaskView.mainView.image.apply( SourceView.image );
	MaskView.mainView.endProcess();

    // *****************************************
    // Dim Brighter Areas Of Image
    // *****************************************
	var hdrmt = new HDRMultiscaleTransform;
	with ( hdrmt ) {
		numberOfLayers = 3;
		numberOfIterations = 1;
		invertedIterations = true;
		overdrive = 0.000;
		medianTransform = false;
		scalingFunctionData = [
		   0.003906,0.015625,0.023438,0.015625,0.003906,
		   0.015625,0.0625,0.09375,0.0625,0.015625,
		   0.023438,0.09375,0.140625,0.09375,0.023438,
		   0.015625,0.0625,0.09375,0.0625,0.015625,
		   0.003906,0.015625,0.023438,0.015625,0.003906
		];
		scalingFunctionRowFilter = [
		   0.0625,0.25,
		   0.375,0.25,
		   0.0625
		];
		scalingFunctionColFilter = [
		   0.0625,0.25,
		   0.375,0.25,
		   0.0625
		];
		scalingFunctionName = "B3 Spline (5)";
		deringing = false;
		smallScaleDeringing = 0.000;
		largeScaleDeringing = 0.250;
		outputDeringingMaps = false;
		midtonesBalanceMode = HDRMultiscaleTransform.prototype.Automatic;
		midtonesBalance = 0.500000;
		toLightness = false;
		preserveHue = false;
		luminanceMask = false;
	}
	hdrmt.executeOn(TargetCloneView.mainView,false);


    // *****************************************
    // Generate Star Mask From Target Copy
    // *****************************************
	var starmask = new StarMask;
	with ( starmask ) {
		shadowsClipping = 0.00000;
		midtonesBalance = 0.25000;
		highlightsClipping = 1.00000;
		waveletLayers = data.SMScale;
		structureContours = true;
		noiseThreshold = data.SMThreshold;
		aggregateStructures = false;
		binarizeStructures = false;
		largeScaleGrowth = 0;
		smallScaleGrowth = 0;
		growthCompensation = 2;
		smoothness = data.SMSmoothness;
		invert = false;
		truncation = 1.00000;
		limit = 1.00000;
		mode = StarMask;
	}
	starmask.executeOn(TargetCloneView.mainView,false);
	
	// *** Get generated star_mask from workspace 
	var window = ImageWindow.windowById("star_mask");												// Get access to the star_mask image
	if ( window.isNull ) {throw Error( "Internal error: Star mask cannot be found" ); return;}		// Handle error if star_mask cannot be found
		
	// Copy the star mask image
	MaskView.mainView.beginProcess( UndoFlag_NoSwapFile );
	MaskView.mainView.image.assign( window.mainView.image );
	MaskView.mainView.endProcess();
		
	// *** Close star_mask window in workspace	
	window.close();
								
    // *****************************************
    // Reduce Stars
    // *****************************************
    // *** Add Processed Star Mask To Target
    TargetView.maskVisible = true;
    TargetView.maskInverted = false;
    TargetView.mask = MaskView;
    TargetView.maskEnabled = true;

    // *** Morphological Transform Target To Reduce Star Size
	var mt = new MorphologicalTransformation;
	with ( mt ) {
		amount = 1.0;
		highThreshold = 0.000000;
		interlacingDistance = 1;
		lowThreshold = 0.000000;
		numberOfIterations = 5;
	  	operator = Selection;
		selectionPoint = 0.20;
		structureName = "5 x 5 One-Way Structure";
		structureSize = 5;
		structureWayTable = [ 			// 5 x 5 Circular One-Way Mask
		   [[
			  0x00,0x01,0x01,0x01,0x00,
			  0x01,0x01,0x01,0x01,0x01,
			  0x01,0x01,0x01,0x01,0x01,
			  0x01,0x01,0x01,0x01,0x01,
			  0x00,0x01,0x01,0x01,0x00
		   ]]
		];
	}
	
    mt.executeOn(TargetView.mainView,false);

    // *** Remove Mask
    TargetView.removeMask();
    MaskView.removeMaskReferences();

    // *****************************************
    // Sharpen Reduced Stars
    // *****************************************
	if(data.optSharpen) {
		// *** Generate Star Mask
		var atwt = new ATrousWaveletTransform;
		with ( atwt ) {
			layers = [ // enabled, biasEnabled, bias, noiseReductionEnabled, noiseReductionThreshold, noiseReductionAmount, noiseReductionIterations
			   [true, true, 0.100, false, 3.000, 1.00, 1],
			   [false, true, 0.000, false, 3.000, 1.00, 1],
			   [false, true, 0.000, false, 3.000, 1.00, 1],
			   [false, true, 0.000, false, 3.000, 1.00, 1],
			   [false, true, 0.000, false, 3.000, 1.00, 1]
			];
			scaleDelta = 0;
			scalingFunctionData = [
			   0.25,0.5,0.25,
			   0.5,1,0.5,
			   0.25,0.5,0.25
			];
			scalingFunctionRowFilter = [
			   0.5,
			   1,
			   0.5
			];
			scalingFunctionColFilter = [
			   0.5,
			   1,
			   0.5
			];
			scalingFunctionNoiseSigma = [
			   0.8003,0.2729,0.1198,
			   0.0578,0.0287,0.0143,
			   0.0072,0.0036,0.0019,
			   0.001
			];
			scalingFunctionName = "Linear Interpolation (3)";
			largeScaleFunction = NoFunction;
			curveBreakPoint = 0.75;
			noiseThresholding = false;
			noiseThresholdingAmount = 1.00;
			noiseThreshold = 3.00;
			softThresholding = true;
			useMultiresolutionSupport = false;
			deringing = false;
			deringingDark = 0.1000;
			deringingBright = 0.0000;
			outputDeringingMaps = false;
			lowRange = 0.0000;
			highRange = 0.0000;
			previewMode = Disabled;
			previewLayer = 0;
			toLuminance = true;
			toChrominance = true;
			linear = false;
		}
   		atwt.executeOn(TargetCloneView.mainView,false);

    	// *** Convert mask to grayscale
    	if(TargetCloneView.mainView.image.colorSpace != ColorSpace_Gray) {
        	var toGray = new ConvertToGrayscale;
        	toGray.executeOn(TargetCloneView.mainView, false);
    	}

		// *** Add Processed Star Mask To Target
		TargetView.maskVisible = true;
		TargetView.maskInverted = false;
		TargetView.mask = TargetCloneView;
		TargetView.maskEnabled = true;
	
		// *** Sharpen Masked Stars
		var atwt = new ATrousWaveletTransform;
		with ( atwt ) {
			layers = [ // enabled, biasEnabled, bias, noiseReductionEnabled, noiseReductionThreshold, noiseReductionAmount, noiseReductionIterations
			   [true, true, 0.000, false, 3.000, 1.00, 1],
			   [true, true, 0.500, false, 3.000, 1.00, 1],
			   [true, true, 0.000, false, 3.000, 1.00, 1],
			   [true, true, 0.000, false, 3.000, 1.00, 1],
			   [true, true, 0.000, false, 3.000, 1.00, 1]
			];
			scaleDelta = 0;
			scalingFunctionData = [
			   0.25,0.5,0.25,
			   0.5,1,0.5,
			   0.25,0.5,0.25
			];
			scalingFunctionRowFilter = [
			   0.5,
			   1,
			   0.5
			];
			scalingFunctionColFilter = [
			   0.5,
			   1,
			   0.5
			];
			scalingFunctionNoiseSigma = [
			   0.8003,0.2729,0.1198,
			   0.0578,0.0287,0.0143,
			   0.0072,0.0036,0.0019,
			   0.001
			];
			scalingFunctionName = "Linear Interpolation (3)";
			largeScaleFunction = NoFunction;
			curveBreakPoint = 0.75;
			noiseThresholding = false;
			noiseThresholdingAmount = 1.00;
			noiseThreshold = 3.00;
			softThresholding = true;
			useMultiresolutionSupport = false;
			deringing = false;
			deringingDark = 0.1000;
			deringingBright = 0.0000;
			outputDeringingMaps = false;
			lowRange = 0.0000;
			highRange = 0.0000;
			previewMode = Disabled;
			previewLayer = 0;
			toLuminance = true;
			toChrominance = false;
			linear = false;
		}
   		atwt.executeOn(TargetView.mainView,false);

    	// *** Remove Mask
    	TargetView.removeMask();
    	TargetCloneView.removeMaskReferences();
	}

	// *** Generate Statistics
	if(data.optMeasure) {
		MeasureProcess();
	}
	
    // *****************************************
    // Close Images No Longer Required
    // *****************************************
    TargetCloneView.forceClose();
    //TargetCloneView.show();
	if(data.optShowMask) {
    	MaskView.show();
	} else {
    	MaskView.forceClose();
	}

    // *****************************************
    // Display Required Images
    // *****************************************
    TargetView.show();
	console.hide();
}

// ******************************************
// * Methods								*
// ******************************************
// *** Create Target View As Copy of Source Image
function OpenViews() {
	if(!data.TargetValid) {
		SourceView = data.targetView;
		data.keywords = SourceView.window.keywords;
		console.writeln(data.keywords);
				
		TargetView = new ImageWindow( SourceView.image.width,
										  SourceView.image.height,
										  SourceView.image.numberOfChannels,
										  SourceView.window.bitsPerSample,
										  SourceView.window.isFloatSample,
										  SourceView.image.colorSpace != ColorSpace_Gray,
										  SourceView.id + "_SR" );
		TargetView.mainView.beginProcess(UndoFlag_NoSwapFile);
		TargetView.mainView.image.apply( SourceView.image );
		TargetView.mainView.endProcess();
		
		var name = 'HISTORY';
		var value = '';
		var comment = 'StarReduction v' + VERSION;
		data.keywords.push( new FITSKeyword( name.toString(), value.toString(), comment.toString() ) );

		TargetView.keywords = data.keywords;
		data.TargetValid = true;
	}
}

// *** Measure FWHM Of Image
function MeasureProcess() {
	disable();
	console.show();
	dialog.infoWindow.text = format( "Measuring FWHM, please wait...");

	OpenViews();
	
	var starProfiles = starProfilesOfImageWindow(TargetView);

	if (starProfiles.length == 0) {
		dialog.infoWindow.text = format( "Cannot measure FWHM as no stars found");
		enable();
		return;
	}
	
	var FWHMs = new Array();
	var eccentricities = new Array();
	var residuals = new Array();
	for (var i = 0; i != starProfiles.length; ++i) {
		var starProfile = starProfiles[i];
		FWHMs.push(Math.sqrt(starProfile.sx * starProfile.sy));
		eccentricities.push(Math.sqrt(1.0 - Math.pow(starProfile.sy / starProfile.sx, 2.0)));
		residuals.push(starProfile.residual);
	}

   	data.validResults = true;
   	var medianMADMedianFWHM = medianMADMedianOfArray(FWHMs);
   	data.medianFWHM = data.modelFunctionNormalizations[data.modelFunctionIndex] * medianMADMedianFWHM.first;
   	data.MADMedianFWHM = data.modelFunctionNormalizations[data.modelFunctionIndex] * medianMADMedianFWHM.last;
   	var medianMADMedianEccentricity = medianMADMedianOfArray(eccentricities);
   	data.medianEccentricity = medianMADMedianEccentricity.first;
   	data.MADMedianEccentricity = medianMADMedianEccentricity.last;
   	var medianMADMedianResidual = medianMADMedianOfArray(residuals);
   	data.medianResidual = data.resolution * medianMADMedianResidual.first;
   	data.MADMedianResidual = data.resolution * medianMADMedianResidual.last;
    data.starSupport = starProfiles.length;
   	data.starProfiles = starProfiles;

	dialog.infoWindow.text = "Median FWHM " + format("%.03f",data.medianFWHM) + " px, MAD FWHM " + format("%.03f",data.MADMedianFWHM) + " px";
	console.writeln();
	console.writeln("Image Statistics for ", TargetView.mainView.id, "<br />",
					"Median FWHM ", format("%.3f",data.medianFWHM), " px<br />",
					"MAD FWHM ", format("%.04f",data.MADMedianFWHM), " px<br />",
					"Median Eccentricity ", format("%.04f",data.medianEccentricity), "<br />",
					"MAD Eccentricity ", format("%.04f",data.MADMedianEccentricity), "<br />",
					"Median Residual ", format("%d",data.medianResidual), " DN<br />",
					"MAD Residual ", format("%d",data.MADMedianResidual), " DN<br />",
					"Star Support ", format("%d",data.starSupport), "<br />"
				);
	
	console.hide();
	enable();
}

function enable() {
	dialog.measure_Button.enabled = true;
	dialog.ok_Button.enabled = true;
	dialog.cancel_Button.enabled = true;
	dialog.browseDocumentation_Button.enabled = true;
	dialog.reset_Button.enabled = true;
}

function disable() {
	dialog.measure_Button.enabled = false;
	dialog.ok_Button.enabled = false;
	dialog.cancel_Button.enabled = false;
	dialog.browseDocumentation_Button.enabled = false;
	dialog.reset_Button.enabled = false;
}

function starProfilesOfImageWindow(imageWindow) {
   var starsViewId = uniqueViewIdNoLeadingZero(imageWindow.mainView.id + "_stars");

   var barycenters = new Array;
   var threshold = 1.0;
   var maximumDetectedStars = 100000;

   console.writeln();
   console.writeln();
   console.writeln("<b>StarDetector ", #__PJSR_STAR_DETECTOR_VERSION, "</b>: Processing view: ", imageWindow.mainView.fullId);
   console.flush();
   var startTime = new Date();

   var starDetector = new StarDetector();
   starDetector.sensitivity = Math.pow(10.0, data.logStarDetectionSensitivity);
   starDetector.upperLimit = data.upperLimit;

   var stars = starDetector.stars(imageWindow.mainView.image);

   var endTime = new Date();
   console.writeln(stars.length, " star(s) found");
   console.writeln(format("%.03f s", 0.001 * (endTime.getTime() - startTime.getTime())));
   console.flush();

   for (;;) {
   		for (var i = 0; i != stars.length && barycenters.length != maximumDetectedStars; ++i) {
            if (threshold == 1.0 || Math.random() <= threshold) {
               barycenters.push({
                  position: stars[i].pos,
                  radius: Math.max(3, Math.ceil(Math.sqrt(stars[i].size)))
               });
            }
         }
         if (barycenters.length != maximumDetectedStars) {
            break;
         }
         barycenters = new Array;
         threshold = 0.1 * threshold;
   }

   var maximumFittedStars = 20000;
   if (barycenters.length > maximumFittedStars) {
      threshold = 0.1 * threshold;
      for (var i = barycenters.length - 1; i != 0; --i) {
         var j = Math.round(Math.random() * i);
         var x = barycenters[i];
         barycenters[i] = barycenters[j];
         barycenters[j] = x;
      }
      barycenters = barycenters.slice(0, maximumFittedStars);
   }

   var dynamicPSF = new DynamicPSF;

   dynamicPSF.autoPSF = false;
   dynamicPSF.circularPSF = false;
   dynamicPSF.gaussianPSF = data.modelFunctionIndex == 0;
   dynamicPSF.moffatPSF = false;
   dynamicPSF.moffat10PSF = data.modelFunctionIndex == 1;
   dynamicPSF.moffat8PSF = data.modelFunctionIndex == 2;
   dynamicPSF.moffat6PSF = data.modelFunctionIndex == 3;
   dynamicPSF.moffat4PSF = data.modelFunctionIndex == 4;
   dynamicPSF.moffat25PSF = data.modelFunctionIndex == 5;
   dynamicPSF.moffat15PSF = data.modelFunctionIndex == 6;
   dynamicPSF.lorentzianPSF = data.modelFunctionIndex == 7;
   dynamicPSF.regenerate = true;

   var views = new Array;
   views.push(new Array(imageWindow.mainView.id));
   dynamicPSF.views = views;

   var radius = Math.round(0.75 * dynamicPSF.searchRadius);
   var stars = new Array;
   for (var i = 0; i != barycenters.length; ++i) {
      stars.push(new Array(
         0, 0, DynamicPSF.prototype.Star_DetectedOk,
         barycenters[i].position.x - barycenters[i].radius,
         barycenters[i].position.y - barycenters[i].radius,
         barycenters[i].position.x + barycenters[i].radius,
         barycenters[i].position.y + barycenters[i].radius,
         barycenters[i].position.x,
         barycenters[i].position.y
      ));
   }
   dynamicPSF.stars = stars;
   var fitted = new Array(stars.length);
   for (var i = 0; i != fitted.length; ++i) {
      fitted[i] = false;
   }
   dynamicPSF.executeGlobal();

   #define DYNAMICPSF_PSF_StarIndex 0
   #define DYNAMICPSF_PSF_Status 3
   #define DYNAMICPSF_PSF_b 4
   #define DYNAMICPSF_PSF_a 5
   #define DYNAMICPSF_PSF_cx 6
   #define DYNAMICPSF_PSF_cy 7
   #define DYNAMICPSF_PSF_sx 8
   #define DYNAMICPSF_PSF_sy 9
   #define DYNAMICPSF_PSF_theta 10
   #define DYNAMICPSF_PSF_residual 12

   var starProfiles = new Array;
   var psfTable = dynamicPSF.psf;
   var starsTable = dynamicPSF.stars;
   for (var i = 0; i != psfTable.length; ++i) {
      var psfRow = psfTable[i];
      if (
         psfRow[DYNAMICPSF_PSF_Status] == DynamicPSF.prototype.PSF_FittedOk &&
         psfRow[DYNAMICPSF_PSF_residual] < 0.1 &&
         !fitted[psfRow[DYNAMICPSF_PSF_StarIndex]]
      ) {
         var starsRow = starsTable[psfRow[DYNAMICPSF_PSF_StarIndex]];
         starProfiles.push(new starProfile(
            psfRow[DYNAMICPSF_PSF_b],
            psfRow[DYNAMICPSF_PSF_a],
            psfRow[DYNAMICPSF_PSF_cx],
            psfRow[DYNAMICPSF_PSF_cy],
            psfRow[DYNAMICPSF_PSF_sx],
            psfRow[DYNAMICPSF_PSF_sy],
            psfRow[DYNAMICPSF_PSF_theta],
            psfRow[DYNAMICPSF_PSF_residual]
         ));
         fitted[psfRow[DYNAMICPSF_PSF_StarIndex]] = true;
      }
   }
   return uniqueArray(starProfiles, starProfileCompare);

}

function uniqueArray(values, compareFunction) {
   if (values.length < 2) {
      return values;
   }
   values.sort(compareFunction);

   var j = 0;
   for (var i = 1; i != values.length; ++i) {
      if (compareFunction(values[j], values[i]) == -1) {
         ++j;
         values[j] = values[i];
      }
   }
   return values.slice(0, j + 1);
}

function starProfile(b, a, x, y, sx, sy, theta, residual) {
   this.b = b;
   this.a = a;
   this.x = x;
   this.y = y;
   this.sx = sx;
   this.sy = sy;
   this.theta = theta;
   this.residual = residual;
}

function starProfileCompare(a, b) {
   var ax = Math.round(a.x);
   var ay = Math.round(a.y);
   var bx = Math.round(b.x);
   var by = Math.round(b.y);
   return ax < bx ? -1 : ax > bx ? 1 : ay < by ? -1 : ay > by ? 1 : 0;
}

function uniqueViewIdNoLeadingZero(baseId) {
   var id = baseId;
   for (var i = 1; !View.viewById(id).isNull; ++i) {
      id = baseId + format("%d", i);
   }
   return id;
}

function Pair(first, last) {
   this.first = first;
   this.last = last;
}

function medianCompare(a, b) {
   return a < b ? -1 : a > b ? 1 : 0;
}

function medianMADMedianOfArray(array) {
   if (array.length < 1) {
      return new Pair(NaN, NaN);
   }
   if (array.length < 2) {
      return new Pair(array[0], 0.0);
   }
   var copy = new Array();
   for (var i = 0; i != array.length; ++i) {
      copy.push(array[i]);
   }
   copy.sort(medianCompare);

   var threshold = Math.floor(0.5 * copy.length);
   var median = (2 * threshold == copy.length) ?
      0.5 * (copy[threshold - 1] + copy[threshold]) :
      copy[threshold];

   var deviations = new Array(copy.length);
   for (var i = 0; i != copy.length; ++i) {
      deviations[i] = Math.abs(median - copy[i]);
   }

   var MADMedian = 0.0;
   for (var i = 0; i != deviations.length; ++i) {
      MADMedian += deviations[i];
   }
   MADMedian = MADMedian / deviations.length;

   return new Pair(median, MADMedian);
}

// ******************************************
// * Reset To Default						*
// ******************************************
function DialogReset() {
	dialog.infoWindow.text = format( "Parameters reset");
	dialog.SMThreshold.setValue(data.defSMThreshold);
	dialog.SMScale.setValue(data.defSMScale);
	dialog.SMSmoothness.setValue(data.defSMSmoothness);
	dialog.FMSensitivity.setValue(data.defLogStarDetectionSensitivity);
	dialog.FMUpperLimit.setValue(data.defUpperLimit);
	dialog.modelFunction.currentItem = data.defModelFunctionIndex;
	dialog.OptSharpen_CheckBox.checked = data.defOptSharpen;
	dialog.OptMeasure_CheckBox.checked = data.defOptMeasure;
	dialog.OptShowMask_CheckBox.checked = data.defOptShowMask;
}

// ******************************************
// * Dialog									*
// ******************************************
function StarReductionDialog() {
   this.__base__ = Dialog;
   this.__base__();


   var emWidth = this.font.width( 'M' );
   var labelWidth = this.font.width( "Convolution Std Dev:" + 'T' );

	// *****************************************
	// Help Text
	// *****************************************
	if(DEFAULT_SHOW_HELP) {
	   this.helpLabel = new Label( this );
	   this.helpLabel.frameStyle = FrameStyle_Box;
	   this.helpLabel.margin = 4;
	   this.helpLabel.wordWrapping = true;
	   this.helpLabel.useRichText = true;
	   this.helpLabel.text =
		    "<p><b>StarReduction v" + VERSION + "</b> &mdash; Reduces the size of stars and optionally sharpens the stars after reduction.</p> "
		  + "<p>Copyright &copy; 2014 Dave Watson. All rights reserved.</p>";
	}

   // Target Image
   this.targetImage_Label = new Label( this );
   this.targetImage_Label.minWidth = 6; // Align with labels inside group boxes below
   this.targetImage_Label.text = "Target image:";
   this.targetImage_Label.textAlignment = TextAlign_Right|TextAlign_VertCenter;

   this.targetImage_ViewList = new ViewList( this );
   this.targetImage_ViewList.minWidth = 200;
   this.targetImage_ViewList.getAll(); 					// Include main views as well as previews
   this.targetImage_ViewList.currentView = data.targetView;
   this.targetImage_ViewList.toolTip = "Select the image to perform the Dark Structure Enhancement.";
   this.targetImage_ViewList.onViewSelected = function( view ) {
      data.targetView = view;
   };

   this.targetImage_Sizer = new HorizontalSizer;
   this.targetImage_Sizer.spacing = 4;
   this.targetImage_Sizer.add( this.targetImage_Label );
   this.targetImage_Sizer.add( this.targetImage_ViewList, 100 );

   // *****************************************
   // Star Mask Parameters
   // *****************************************
   this.SMThreshold = new NumericControl (this);
   with ( this.SMThreshold ) {
      label.text = "Threshold:";
      label.minWidth = labelWidth;
      setRange (data.minSMThreshold, data.maxSMThreshold);
      slider.setRange (0, 1000);
      slider.minWidth = 250;
      setPrecision (2);
      setValue (data.SMThreshold);
      toolTip = "<p>Threshold isolates noise from valid stucture, increasing value will discriminate smaller structure and exclude smaller stars from mask.</p>";
      onValueUpdated = function (value) { data.SMThreshold = value; };
   }
   
   this.SMScale = new NumericControl (this);
   with ( this.SMScale ) {
      label.text = "Scale:";
      label.minWidth = labelWidth
      setRange ( data.minSMScale, data.maxSMScale);
      slider.setRange (0, 1000);
      slider.minWidth = 250;
      setPrecision (0);
      setValue (data.SMScale);
      toolTip = "<p>Wavelet layers for structure detection, increase scale to include larger structures.</p>";
      onValueUpdated = function (value) { data.SMScale = value; };
   }
   
   this.SMSmoothness = new NumericControl (this);
   with ( this.SMSmoothness ) {
      label.text = "Smoothness:";
      label.minWidth = labelWidth;
      setRange (data.minSMSmoothness, data.maxSMSmoothness);
      slider.setRange (0, 1000);
      slider.minWidth = 250;
      setPrecision (0);
      setValue (data.SMSmoothness);
      toolTip = "<p>Smoothnes of mask structure, insufficient will lead to edge artifacts, excessive will degrade protection. The default value gives a fair but not overwhelming smoothing of the stars.</p>";
      onValueUpdated = function (value) { data.SMSmoothness = value; };
   }
   
  this.starMaskGroupBox = new GroupBox( this );
   with ( this.starMaskGroupBox ) {
      title = "Star Mask Parameters";
      sizer = new VerticalSizer;
      sizer.margin = 6;
      sizer.spacing = 4;
      sizer.add(this.SMThreshold);
      sizer.add(this.SMScale);
      sizer.add(this.SMSmoothness);
   }

   // *****************************************
   // FWHM Measurment Parameters
   // *****************************************
   this.FMSensitivity = new NumericControl (this);
   with ( this.FMSensitivity ) {
      label.text = "Detection sensitivity:";
      label.minWidth = labelWidth
      setRange (data.minLogStarDetectionSensitivity, data.maxLogStarDetectionSensitivity);
      slider.setRange (0, 1000);
      slider.minWidth = 250;
      setPrecision (2);
      setValue (data.logStarDetectionSensitivity);
      toolTip = "<p>This parameter specifies the logarithm of the star detection " +
      "sensitivity.</p>" +
      "<p>Decrease this parameter to favor detection of fainter stars or stars on " +
      "brighter backgrounds. Increase it to restrict detection to brighter stars or stars " +
      "on dimmer backgrounds.</p>";
      onValueUpdated = function (value) { data.logStarDetectionSensitivity = value; };
   }

   this.FMUpperLimit = new NumericControl (this);
   with ( this.FMUpperLimit ) {
      label.text = "Upper limit:";
      label.minWidth = labelWidth;
      setRange (data.minUpperLimit,data.maxUpperLimit);
      slider.setRange (0, 1000);
      slider.minWidth = 250;
      setPrecision (2);
      setValue (data.upperLimit);
      toolTip = "<p>Stars with peak values larger than this value won't be measured.</p>" +
      "<p>This feature may be used to avoid the measurement of saturated and bloomed stars.</p>" +
      "<p>To disable this feature, set this parameter to one.</p>";
      onValueUpdated = function (value) { data.upperLimit = value; };
   }
   
   this.modelFunctionPane = new HorizontalSizer;
   this.modelFunctionPane.spacing = 6;
   this.modelFunctionLabel = new Label(this);

   this.modelFunctionLabel.setFixedWidth(labelWidth);
   this.modelFunctionLabel.text = "Model function:";
   this.modelFunctionLabel.textAlignment = TextAlign_Right | TextAlign_VertCenter;
   this.modelFunctionLabel.toolTip = "<p>This parameters specifies the star profile model function " +
      "used to fit star images.</p>";
   this.modelFunctionPane.add(this.modelFunctionLabel);

   this.modelFunction = new ComboBox(this);
   for (var i = 0; i != data.modelFunctionLabels.length; ++i) {
      this.modelFunction.addItem(" " + data.modelFunctionLabels[i]);
   }
   this.modelFunction.currentItem = data.modelFunctionIndex;
   this.modelFunction.toolTip = this.modelFunctionLabel.toolTip;
   this.modelFunction.onItemSelected = function(item) {
      if (data.modelFunctionIndex != item) {
         data.modelFunctionIndex = item;
      }
   };
   this.modelFunctionPane.add(this.modelFunction);
   this.modelFunctionPane.addStretch();
   
   this.measurementGroupBox = new GroupBox( this );
   with ( this.measurementGroupBox ) {
      title = "FWHM Measurement Parameters";
      sizer = new VerticalSizer;
      sizer.margin = 6;
      sizer.spacing = 4;
      sizer.add(this.FMSensitivity);
      sizer.add(this.FMUpperLimit);
      sizer.add(this.modelFunctionPane);
   }

   // *****************************************
   // Options
   // *****************************************
   this.OptMeasure_CheckBox = new CheckBox( this );
   this.OptMeasure_CheckBox.text = "Measure FWHM after reduction";
   this.OptMeasure_CheckBox.checked = data.optMeasure;
   this.OptMeasure_CheckBox.toolTip =
      "<p>If this option is selected, the script will measure the FWHM of the "
      + "start reduced image and list the measurements in the Process Console.</p>";
   this.OptMeasure_CheckBox.onCheck = function( checked ) {
      data.optMeasure = checked;
   };

   this.OptSharpen_CheckBox = new CheckBox( this );
   this.OptSharpen_CheckBox.text = "Sharpen Stars after reduction";
   this.OptSharpen_CheckBox.checked = data.optSharpen;
   this.OptSharpen_CheckBox.toolTip =
      "<p>If this option is selected, the script will sharpen the star reduced image.</p>"
   this.OptSharpen_CheckBox.onCheck = function( checked ) {
      data.optSharpen = checked;
   };

   this.OptShowMask_CheckBox = new CheckBox( this );
   this.OptShowMask_CheckBox.text = "Show mask";
   this.OptShowMask_CheckBox.checked = data.optShowMask;
   this.OptShowMask_CheckBox.toolTip =
      "<p>If this option is selected, the script will display the generated mask.</p>";
   this.OptShowMask_CheckBox.onCheck = function( checked ) {
      data.optShowMask = checked;
   };

   this.optionsGroupBox = new GroupBox( this );
   with ( this.optionsGroupBox ) {
      title = "Options";
      sizer = new HorizontalSizer;
      sizer.margin = 6;
      sizer.spacing = 4;
      sizer.add(this.OptSharpen_CheckBox);
      sizer.add(this.OptMeasure_CheckBox);
      sizer.add(this.OptShowMask_CheckBox);
   }
      
   // *****************************************
   // Control buttons
   // *****************************************
   this.browseDocumentation_Button = new ToolButton(this);
   this.browseDocumentation_Button.icon = ":/process-interface/browse-documentation.png";
   this.browseDocumentation_Button.toolTip = "<p>Opens a browser to view the script's documentation</p>";
   this.browseDocumentation_Button.onClick = function () {
      if (!Dialog.browseScriptDocumentation("StarReduction")) {
         (new MessageBox(
            "<p>Documentation has not been installed.</p>" +

            "<p>This script reduces the size of the stars in the selected image and optionally sharpens the stars after reduction. The script is " +
			"designed to processes non-linear grayscale images that have have been cropped, processed with Dynamic Background Extraction (DBE), and initially stretched with Histogram Transform.</p>" +

            "<p>The default settings of the parameters have been determined to give the best results, however, they may need slight adjustment to suit individual images." +
			" Parameters are available to control the star mask and FWHM measurement processes.</p>" +
			
            "<p>Copyright &copy; 2014-2015 Dave Watson. All Rights Reserved.<br/>" + "</p>",

            TITLE + "." + VERSION,
            StdIcon_NoIcon,
            StdButton_Ok
         )).execute();
      }
   };

   this.reset_Button = new ToolButton(this);
   this.reset_Button.icon =  ":/images/icons/reset.png";
   this.reset_Button.toolTip = "<p>Resets the dialog's parameters";
   this.reset_Button.onClick = function() {
     	DialogReset();
   };

    this.measure_Button = new PushButton( this );
    this.measure_Button.icon = ":/icons/gear.png";
    this.measure_Button.text = "Measure FWHM";
    this.measure_Button.onClick = function() {
        MeasureProcess();
    };

    this.ok_Button = new PushButton( this );
    this.ok_Button.text = "Run";
    this.ok_Button.icon = ":/icons/power.png";
    this.ok_Button.onClick = function() {
        this.dialog.ok();
    };

    this.cancel_Button = new PushButton( this );
    this.cancel_Button.text = "Exit";
    this.cancel_Button.icon = ":/icons/close.png";
    this.cancel_Button.onClick = function() {
		if(data.TargetValid) {TargetView.forceClose();}
        this.dialog.cancel();
    };

    this.buttons_Sizer = new HorizontalSizer;
    this.buttons_Sizer.spacing = 4;
    this.buttons_Sizer.add( this.browseDocumentation_Button );
    this.buttons_Sizer.add( this.reset_Button );
    this.buttons_Sizer.addStretch();
    this.buttons_Sizer.add( this.measure_Button );
    this.buttons_Sizer.add( this.ok_Button );
    this.buttons_Sizer.add( this.cancel_Button );

   this.infoWindow = new Label( this );
   with ( this.infoWindow ) {
      minWidth = 450;
      frameStyle = FrameStyle_Box;
      margin = 4;
      text = "Ready";
   }

   this.info_Sizer = new HorizontalSizer;
   with ( this.info_Sizer ) {
      spacing = 4;
      addStretch();
      add (this.infoWindow);
   }

   // *****************************************
   // Display Dialogue
   // *****************************************
    this.sizer = new VerticalSizer;
    this.sizer.margin = 8;
    this.sizer.spacing = 6;
	if(DEFAULT_SHOW_HELP) {
    	this.sizer.add( this.helpLabel );
	}
    this.sizer.addSpacing( 4 );
    this.sizer.add( this.targetImage_Sizer );
    this.sizer.add( this.starMaskGroupBox );
	this.sizer.add( this.measurementGroupBox );
    this.sizer.add( this.optionsGroupBox );
    this.sizer.addSpacing( 4 );
    this.sizer.add( this.info_Sizer );
    this.sizer.addSpacing( 4 );
    this.sizer.add( this.buttons_Sizer );

    this.windowTitle = TITLE;
    this.adjustToContents();
    this.setFixedSize();
	
	dialog = this;
}

var dialog;
StarReductionDialog.prototype = new Dialog;

// ******************************************
// * Script Entry Point						*
// ******************************************
function main() {
   console.hide();


   if ( !data.targetView ) {
      var msg = new MessageBox( "There is no active image window!",TITLE,StdIcon_Error, StdButton_Ok );
      msg.execute();
      return;
   }

   // We only work with grayscale images
   if (DEFAULT_WARNING_RGB && !data.targetView.image.isGrayscale) {
      (new MessageBox(
         "<p>Target view color space must be Grayscale for correct operation</p>",
         TITLE + "." + VERSION,
         StdIcon_Warning,
         StdButton_Ok
      )).execute();
   }
   
   dialog = new StarReductionDialog();
   for ( ;; ) {
      if ( !dialog.execute() )
         break;

      // A view must be selected.
      if ( data.targetView.isNull ) {
         var msg = new MessageBox( "You must select a view to apply this script.",TITLE, StdIcon_Error, StdButton_Ok );
         msg.execute();
         continue;
      }

      console.show();
      StarReductionProcess( data );
      break;
   }
}

main();
// ****************************************************************************
// pjsr/StarReduction.js
// ****************************************************************************
