// ****************************************************************************
// pjsr/LargeScaleStructureEnhance.js - Released 01/01/2015
// Version: 1.01
// ****************************************************************************
// Copyright (c) 2014-2015, Dave Watson. All Rights Reserved.
// Website: http://www.qdigital-astro.com
// Email:   dave@qdigital-imaging.com
//
// Redistribution and use in both source and binary forms, with or without
// modification, is permitted provided that the following conditions are met:
//
// 1. All redistributions of source code must retain the above copyright
//    notice, this list of conditions and the following disclaimer.
//
// 2. All redistributions in binary form must reproduce the above copyright
//    notice, this list of conditions and the following disclaimer in the
//    documentation and/or other materials provided with the distribution.
//
// 3. All products derived from this software, in any form whatsoever, must
//    reproduce the following acknowledgment in the end-user documentation
//    and/or other materials provided with the product.
//
// 4. This product is based on software from the PixInsight project, developed
//    by Pleiades Astrophoto and its contributors (http://pixinsight.com/).
//
// 	  This file executes within the PixInsight JavaScript Runtime (PJSR).
// 	  PJSR is an ECMA-262 compliant framework for development of scripts on the
//    PixInsight platform.
//    Copyright (c) 2003-2014, Pleiades Astrophoto S.L. All Rights Reserved.
//
// THIS SOFTWARE IS PROVIDED BY DAVE WATSON "AS IS" AND ANY EXPRESS OR IMPLIED 
// WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF 
// MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
// IN NO EVENT SHALL DAVE WATSON BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
// SPECIAL, EXEMPLARY OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, 
// BUSINESS INTERRUPTION; PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; AND LOSS
// OF USE, DATA OR PROFITS) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER
// IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
// ****************************************************************************

/*
 * LargeScaleStructureEnhance.js
 *
 * A utility script for the enhancement of faint large structures such as Integrated Flux Nebulae (IFN) which are very close to the noise floor.
 * This script is based on development work by Vincent Peris and the post-processing workflow as described by Rogelio Bernal Andreo
 * in the book Lessions From The Masters edited by Robert Gendler.
 * The script is designed to processes non-linear monochrome images that have have been cropped, processed with Dynamic Background Extraction (DBE),
 * and initially stretched with Histogram Transformation.
 *
 * This script incorporates modified and simplified code from the AutoSTF script from Juan Conejero.
 *
 */
 
#define __DEBUG     false

#include <pjsr/Sizer.jsh>
#include <pjsr/FrameStyle.jsh>
#include <pjsr/TextAlign.jsh>
#include <pjsr/StdButton.jsh>
#include <pjsr/StdIcon.jsh>
#include <pjsr/UndoFlag.jsh>
#include <pjsr/ColorSpace.jsh>
#include <pjsr/NumericControl.jsh>
#include <pjsr/SampleType.jsh>

#feature-id    Utilities > LargeScaleStructureEnhance

#feature-info  A script for enhancement of large scale structures.<br/>\
   <br/>\
   A utility script for the enhancement of faint large structures such as Integrated Flux Nebulae (IFN) which are very close to the noise floor. \
   Based on an original workflow created by Rogelio Bernal Andreo and described in the publication <i>Lessons from the Masters</i> by Robert Gendler. \
   JavaScript/PixInsight implementation by Dave Watson (PixInsight User).<br/> \
   <br/> \
   Copyright &copy; 2014-2015 Dave Watson

#feature-icon  LargeScaleStructureEnhance.xpm

#define VERSION "1.01"

#define TITLE "LargeScaleStructureEnhance v" + VERSION

/*
 * Default Parameters
 */
#define DEFAULT_SHOW_HELP			false		// true = show help text, false = don't show
#define DEFAULT_STARMASK_ATWT   	false		// true = AWTWT, false = StarMask function
#define DEFAULT_WARNING_RGB		   	false		// true = warn if RGB image, false = no warning
#define DEFAULT_AUTOSTRETCH_SCLIP  -2.80		// Shadows clipping point in (normalized) MAD units from the median
#define DEFAULT_AUTOSTRETCH_TBGND   0.25		// Target mean background in the [0,1] range
#define VERBOSE						true		// Verbose mode if true

var window = ImageWindow.activeWindow;

// ******************************************
// * Data									*
// ******************************************
function LargeScaleStructureEnhanceData() {
    // Get access to the active image window
    if (!window.isNull) {this.targetView = window.currentView;}

	// Set default parameters
	this.defViewLS = true;
	this.viewLS = this.defViewLS;
	
	this.defViewSS = true;
	this.viewSS = this.defViewSS;
	
	this.defViewMS = false;
	this.viewMS = this.defViewMS;
	
	this.defViewFI = false;
	this.viewFI = this.defViewFI;
	
	this.defHtSS = false;
	this.htSS = this.defHtSS;
	
	this.defHtLS = false;
	this.htLS = this.defHtLS;
	
	this.minSMThreshold = 0.5;
	this.maxSMThreshold = 0.5;
	this.defSMThreshold = 0.5;
	this.SMThreshold = this.defSMThreshold;
	
	this.minSMScale = 9;
	this.maxSMScale = 9;
	this.defSMScale = 9;
	this.SMScale = this.defSMScale;
	
	this.minSMSmoothness = 10;
	this.maxSMSmoothness = 10;
	this.defSMSmoothness = 10;
	this.SMSmoothness = this.defSMSmoothness;
	
	this.minSMSigma = 25;
	this.maxSMSigma = 25;
	this.defSMSigma = 25;
	this.SMSigma = this.defSMSigma;
	
	this.stat = new Array();   // Array of statistics for auto-stretch functions
}

var data = new LargeScaleStructureEnhanceData;

// ******************************************
// * Process								*
// ******************************************
function LargeScaleStructureEnhanceProcess(data) {
	var SourceView = data.targetView;
	
	var TargetView = new ImageWindow(	 SourceView.image.width,
									 	 SourceView.image.height,
										 SourceView.image.numberOfChannels,
										 SourceView.window.bitsPerSample,
									 	 SourceView.window.isFloatSample,
									 	 SourceView.image.colorSpace != ColorSpace_Gray,
									  	 "TargetView" );
	TargetView.mainView.beginProcess(UndoFlag_NoSwapFile);
	TargetView.mainView.image.assign( SourceView.image );
	TargetView.mainView.endProcess();

	var LSView = new ImageWindow( 		 SourceView.image.width,
								  		 SourceView.image.height,
								  		 SourceView.image.numberOfChannels,
								  		 SourceView.window.bitsPerSample,
								  		 SourceView.window.isFloatSample,
								  		 SourceView.image.colorSpace != ColorSpace_Gray,
								  		 "LSView" );
	LSView.mainView.beginProcess(UndoFlag_NoSwapFile);
	LSView.mainView.image.assign( SourceView.image );
	LSView.mainView.endProcess();

	var SSView = new ImageWindow( 		 SourceView.image.width,
								  		 SourceView.image.height,
								  		 SourceView.image.numberOfChannels,
								  		 SourceView.window.bitsPerSample,
								  		 SourceView.window.isFloatSample,
								  		 SourceView.image.colorSpace != ColorSpace_Gray,
								  		 "SSView" );
	SSView.mainView.beginProcess(UndoFlag_NoSwapFile);
	SSView.mainView.image.assign( SourceView.image );
	SSView.mainView.endProcess();

	var MaskView = new ImageWindow( 	 SourceView.image.width,
								  		 SourceView.image.height,
								 		 SourceView.image.numberOfChannels,
								  		 SourceView.window.bitsPerSample,
								  		 SourceView.window.isFloatSample,
								  		 SourceView.image.colorSpace != ColorSpace_Gray,
								  		 "MaskView" );
	MaskView.mainView.beginProcess(UndoFlag_NoSwapFile);
	MaskView.mainView.image.assign( SourceView.image );
	MaskView.mainView.endProcess();

	var MaskCloneView = new ImageWindow( SourceView.image.width,
								  		 SourceView.image.height,
								  		 SourceView.image.numberOfChannels,
								  		 SourceView.window.bitsPerSample,
								  		 SourceView.window.isFloatSample,
								  		 SourceView.image.colorSpace != ColorSpace_Gray,
								  		 "MaskCloneView" );
	MaskCloneView.mainView.beginProcess(UndoFlag_NoSwapFile);
	MaskCloneView.mainView.image.assign( SourceView.image );
	MaskCloneView.mainView.endProcess();
					
    // *****************************************
    // Generate Star Mask
    // *****************************************
	// Creates a star mask of the more prominent stars only
	if(!DEFAULT_STARMASK_ATWT) {
		var starmask = new StarMask;
		with ( starmask ) {
		   aggregateStructures = false;
		   binarizeStructures = true;
		   growthCompensation = 2;
		   highlightsClipping = 1;
		   invert = false;
		   largeScaleGrowth = 2;
		   limit = 1;
		   midtonesBalance = 0.9;
		   mode = StarMask;
		   noiseThreshold = data.SMThreshold;
		   shadowsClipping = 0.9;
		   smallScaleGrowth = 0;
		   smoothness = data.SMSmoothness;
		   structureContours = false;
		   truncation = 1;
		   waveletLayers = data.SMScale;
		}
		starmask.executeOn(TargetView.mainView,false);
	
		// *** Get generated star_mask from workspace 
		var window = ImageWindow.windowById("star_mask");								// Get access to the star_mask image
		if ( window.isNull ) {throw Error( "Star mask cannot be found" ); return;}		// Handle error if star_mask cannot be found
		
		// Copy the star mask image
		MaskView.mainView.beginProcess( UndoFlag_NoSwapFile );
		MaskView.mainView.image.assign( window.mainView.image );
		MaskView.mainView.endProcess();
		
		// *** Close star_mask window in workspace	
		window.close();
	} else {
		var starmask = new ATrousWaveletTransform;
		with ( starmask ) {
			layers = [ // enabled, biasEnabled, bias, noiseReductionEnabled, noiseReductionThreshold, noiseReductionAmount, noiseReductionIterations
			   [true, true, 0.100, false, 3.000, 1.00, 1],
			   [true, true, 0.800, false, 3.000, 1.00, 1],
			   [true, true, 0.500, false, 3.000, 1.00, 1],
			   [true, true, 0.000, false, 3.000, 1.00, 1],
			   [true, true, 0.000, false, 3.000, 1.00, 1],
			   [true, true, 0.000, false, 3.000, 1.00, 1],
			   [false, true, 0.000, false, 3.000, 1.00, 1]
			];
			scaleDelta = 0;
			scalingFunctionData = [
			   0.25,0.5,0.25,
			   0.5,1,0.5,
			   0.25,0.5,0.25
			];
			scalingFunctionRowFilter = [
			   0.5,
			   1,
			   0.5
			];
			scalingFunctionColFilter = [
			   0.5,
			   1,
			   0.5
			];
			scalingFunctionNoiseSigma = [
			   0.8003,0.2729,0.1198,
			   0.0578,0.0287,0.0143,
			   0.0072,0.0036,0.0019,
			   0.001
			];
			scalingFunctionName = "Linear Interpolation (3)";
			largeScaleFunction = ATrousWaveletTransform.prototype.NoFunction;
			curveBreakPoint = 0.75;
			noiseThresholding = false;
			noiseThresholdingAmount = 1.00;
			noiseThreshold = 4.70;
			softThresholding = true;
			useMultiresolutionSupport = false;
			deringing = false;
			deringingDark = 0.1000;
			deringingBright = 0.0000;
			outputDeringingMaps = false;
			lowRange = 0.0000;
			highRange = 0.0000;
			previewMode = ATrousWaveletTransform.prototype.Disabled;
			previewLayer = 0;
			toLuminance = true;
			toChrominance = false;
			linear = false;
		}
		starmask.executeOn(MaskView.mainView,false);
	}

    // *** Convert mask to grayscale
    if(MaskView.mainView.image.colorSpace != ColorSpace_Gray) {
        var toGray = new ConvertToGrayscale;
        toGray.executeOn( MaskView.mainView, false);
    }

    // *** Clone Star Mask Image
    MaskCloneView.mainView.beginProcess(UndoFlag_NoSwapFile);
    MaskCloneView.mainView.image.apply(MaskView.mainView.image);
    MaskCloneView.mainView.endProcess();

    // *** Morphological Transform Clone Star Mask To Dim The Brightest Stars
	var mt = new MorphologicalTransformation;
	with ( mt ) {
		amount = 1.0;
		highThreshold = 0.000000;
		interlacingDistance = 1;
		lowThreshold = 0.000000;
		numberOfIterations = 2;
	  	operator = Dilation;
		selectionPoint = 0.50;
		structureName = "15 x 15 One-Way Structure";
		structureSize = 15;
		structureWayTable = [ 			// 15 x 15 Circular One-Way Mask
		   [[
			  0x00,0x00,0x00,0x00,0x00,0x01,0x01,0x01,0x01,0x01,0x00,0x00,0x00,0x00,0x00,
			  0x00,0x00,0x00,0x01,0x01,0x01,0x01,0x01,0x01,0x01,0x01,0x01,0x00,0x00,0x00,
			  0x00,0x00,0x01,0x01,0x01,0x01,0x01,0x01,0x01,0x01,0x01,0x01,0x01,0x00,0x00,
			  0x00,0x01,0x01,0x01,0x01,0x01,0x01,0x01,0x01,0x01,0x01,0x01,0x01,0x01,0x00,
			  0x00,0x01,0x01,0x01,0x01,0x01,0x01,0x01,0x01,0x01,0x01,0x01,0x01,0x01,0x00,
			  0x01,0x01,0x01,0x01,0x01,0x01,0x01,0x01,0x01,0x01,0x01,0x01,0x01,0x01,0x01,
			  0x01,0x01,0x01,0x01,0x01,0x01,0x01,0x01,0x01,0x01,0x01,0x01,0x01,0x01,0x01,
			  0x01,0x01,0x01,0x01,0x01,0x01,0x01,0x01,0x01,0x01,0x01,0x01,0x01,0x01,0x01,
			  0x01,0x01,0x01,0x01,0x01,0x01,0x01,0x01,0x01,0x01,0x01,0x01,0x01,0x01,0x01,
			  0x01,0x01,0x01,0x01,0x01,0x01,0x01,0x01,0x01,0x01,0x01,0x01,0x01,0x01,0x01,
			  0x00,0x01,0x01,0x01,0x01,0x01,0x01,0x01,0x01,0x01,0x01,0x01,0x01,0x01,0x00,
			  0x00,0x01,0x01,0x01,0x01,0x01,0x01,0x01,0x01,0x01,0x01,0x01,0x01,0x01,0x00,
			  0x00,0x00,0x01,0x01,0x01,0x01,0x01,0x01,0x01,0x01,0x01,0x01,0x01,0x00,0x00,
			  0x00,0x00,0x00,0x01,0x01,0x01,0x01,0x01,0x01,0x01,0x01,0x01,0x00,0x00,0x00,
			  0x00,0x00,0x00,0x00,0x00,0x01,0x01,0x01,0x01,0x01,0x00,0x00,0x00,0x00,0x00
		   ]]
		];
	}
	
    mt.executeOn(MaskCloneView.mainView,false);
	
    // *** Add Star Mask To Morphed Star Mask Clone
	var pm = new PixelMath;
	with ( pm ) {
	   expression = "MaskView+MaskCloneView";
	   expression1 = "";
	   expression2 = "";
	   expression3 = "";
	   useSingleExpression = true;
	   symbols = "";
	   use64BitWorkingImage = false;
	   rescale = true;
	   rescaleLower = 0.0000000000;
	   rescaleUpper = 1.0000000000;
	   truncate = false;
	   truncateLower = 0.0000000000;
	   truncateUpper = 1.0000000000;
	   createNewImage = false;
	   newImageId = "";
	   newImageWidth = 0;
	   newImageHeight = 0;
	   newImageAlpha = false;
	   newImageColorSpace = SameAsTarget;
	   newImageSampleFormat = SameAsTarget;
	}
    pm.executeOn(MaskView.mainView,false);

    // *** Delete Mask Clone As It is No Longer Required
	if(__DEBUG) {
		MaskCloneView.show();
	} else {
		MaskCloneView.forceClose();
	}
	
    // *** Convolving Star Mask To Smooth Out Star Mask
	var cv = new Convolution;
	with ( cv ) {
      mode = Parametric;
      sigma = data.SMSigma;					// StdDev
      shape = 6.0;
      aspectRatio = 1.0;
      rotationAngle = 0.0;
	}
    cv.executeOn(MaskView.mainView,false);

    // *****************************************
    // Dim Brighter Stars
    // *****************************************
    // *** Add Processed Star Mask To Copy Of Target (Large Scale View Window)
    LSView.maskVisible = true;
    LSView.maskInverted = false;
    LSView.mask = MaskView;
    LSView.maskEnabled = true;

    // *** Morphological Transform Copy Of Target
	// This dims the brighter stars
	var mt = new MorphologicalTransformation;
	with ( mt ) {
		amount = 1.0;
		highThreshold = 0.000000;
		interlacingDistance = 1;
		lowThreshold = 0.000000;
		numberOfIterations = 5;
	  	operator = Selection;
		selectionPoint = 0.20;
		structureName = "9 x 9 One-Way Structure";
		structureSize = 9;
		structureWayTable = [ 			// 9 x 9 Circular One-Way Mask
		   [[
			  0x00,0x00,0x01,0x01,0x01,0x01,0x01,0x00,0x00,
			  0x00,0x01,0x01,0x01,0x01,0x01,0x01,0x01,0x00,
			  0x01,0x01,0x01,0x01,0x01,0x01,0x01,0x01,0x01,
			  0x01,0x01,0x01,0x01,0x01,0x01,0x01,0x01,0x01,
			  0x01,0x01,0x01,0x01,0x01,0x01,0x01,0x01,0x01,
			  0x01,0x01,0x01,0x01,0x01,0x01,0x01,0x01,0x01,
			  0x01,0x01,0x01,0x01,0x01,0x01,0x01,0x01,0x01,
			  0x00,0x01,0x01,0x01,0x01,0x01,0x01,0x01,0x00,
			  0x00,0x00,0x01,0x01,0x01,0x01,0x01,0x00,0x00
		   ]]
		];
	}
	
    mt.executeOn(LSView.mainView,false);

    // *** Remove Mask
    LSView.removeMask();
    MaskView.removeMaskReferences();

   // *****************************************
   // Break Image Into Different Scales
   // *****************************************
    // *** Multiscale Median Transform Copy Of Target
	var mmt = new MultiscaleMedianTransform;
	with ( mmt ) {
		layers = [ // enabled, biasEnabled, bias, noiseReductionEnabled, noiseReductionThreshold, noiseReductionAmount, noiseReductionAdaptive
		   // 5 layers plus the 'R' layer
		   [false, true, 0.000, false, 1.0000, 1.00, 0.0000],
		   [false, true, 0.000, false, 1.0000, 1.00, 0.0000],
		   [false, true, 0.000, false, 1.0000, 1.00, 0.0000],
		   [false, true, 0.000, false, 1.0000, 1.00, 0.0000],
		   [false, true, 0.000, false, 1.0000, 1.00, 0.0000],
		   [true, true, 0.000, false, 1.0000, 1.00, 0.0000]
		];
		highRange = 0.0;
		linear = false;
		linearMask = false;
		linearMaskAmpFactor = 100;
		linearMaskSmoothness = 1.00;
		lowRange = 0.0;
		medianWaveletThreshold = 5.00;
		previewMode = Disabled;
		toLuminance = true;				// Lightness (CIE L*) = linear = false, toLuminance = true, toChrominance = false
      	toChrominance = false;
		transform = MultiscaleMedianTransform;
	}
	
    mmt.executeOn(LSView.mainView,false);

    // *** Generating Small Scale SSView
	// At this stage the SSView contains a copy of the Target View
    if(__DEBUG) {console.writeln("DEBUG: Generating SSView from LSView and Target Image");}
	var pm = new PixelMath;
	with ( pm ) {
	   expression = "SSView-LSView";
	   expression1 = "";
	   expression2 = "";
	   expression3 = "";
	   useSingleExpression = true;
	   symbols = "";
	   use64BitWorkingImage = false;
	   rescale = true;
	   rescaleLower = 0.0000000000;
	   rescaleUpper = 1.0000000000;
	   truncate = false;
	   truncateLower = 0.0000000000;
	   truncateUpper = 1.0000000000;
	   createNewImage = false;
	   newImageId = "";
	   newImageWidth = 0;
	   newImageHeight = 0;
	   newImageAlpha = false;
	   newImageColorSpace = SameAsTarget;
	   newImageSampleFormat = SameAsTarget;
	}
    pm.executeOn(SSView.mainView,false);
 
    // *****************************************
    // Histogram Stretch Structure Images
    // *****************************************
	if(data.htSS) {this.ApplyAutoHT(SSView.mainView,'SS',VERBOSE,DEFAULT_AUTOSTRETCH_SCLIP,DEFAULT_AUTOSTRETCH_TBGND);}
	if(data.htLS) {this.ApplyAutoHT(LSView.mainView,'LS',VERBOSE,DEFAULT_AUTOSTRETCH_SCLIP,DEFAULT_AUTOSTRETCH_TBGND);}
	 
    // *****************************************
    // Generate Final Image
    // *****************************************
    // *** Add SSView And LSView To Create Final Image
	if (data.viewFI) {
		var pm = new PixelMath;
		with ( pm ) {
		   expression = "SSView+LSView";
		   expression1 = "";
		   expression2 = "";
		   expression3 = "";
		   useSingleExpression = true;
		   symbols = "";
		   use64BitWorkingImage = false;
		   rescale = true;
		   rescaleLower = 0.0000000000;
		   rescaleUpper = 1.0000000000;
		   truncate = false;
		   truncateLower = 0.0000000000;
		   truncateUpper = 1.0000000000;
		   createNewImage = true;
		   newImageId = SourceView.id + "_LSE";
		   newImageWidth = SameAsTarget;
		   newImageHeight = SameAsTarget;
		   newImageAlpha = false;
		   newImageColorSpace = SameAsTarget;
		   newImageSampleFormat = SameAsTarget;
		}
		pm.executeOn(TargetView.mainView,false);
	}

    // *****************************************
    // Close Images No Longer Required
    // *****************************************
    TargetView.forceClose();

    // *****************************************
    // Display Required Images Or Close Them
    // *****************************************
    if (data.viewLS) {LSView.show();} else {LSView.forceClose();}
    if (data.viewSS) {SSView.show();} else {SSView.forceClose();}
    if (data.viewMS) {MaskView.show();} else {MaskView.forceClose();}
}

// ******************************************
// * Methods								*
// ******************************************
/*
 * HT Auto Stretch routine
 */
function ApplyAutoHT(view, mode, verbose, shadowsClipping, targetBackground) {
      var stretch =  this.calculateStretch(view, mode, verbose, shadowsClipping, targetBackground);

      if ( stretch.c0 > 0 || stretch.m != 0.5 || stretch.c1 != 1 ) {
         var HT = new HistogramTransformation;
         HT.H = [
		 			[  0, 0.5,   1, 0, 1],
            		[  0, 0.5,   1, 0, 1],
            		[  0, 0.5,   1, 0, 1],
            		[stretch.c0, stretch.m, stretch.c1, 0, 1],
            		[  0, 0.5,   1, 0, 1]
				];

         HT.executeOn( view, false ); // no swap file
		if (verbose) {
			console.writeln("<end><cbr/><br/><b>", view.fullId, "</b>:");
			console.writeln(format("c0 = %.6f", stretch.c0));
			console.writeln(format("m  = %.6f", stretch.m));
			console.writeln(format("c1 = %.6f", stretch.c1));
			console.writeln("<end><cbr/><br/>");
		}
      }
   }

function calculateStretch(view, mode, verbose, shadowsClipping, targetBackground) {
      if (verbose == undefined)
         verbose = false;
      if (shadowsClipping == undefined)
         shadowsClipping = DEFAULT_AUTOSTRETCH_SCLIP;
      if (targetBackground == undefined)
         targetBackground = DEFAULT_AUTOSTRETCH_TBGND;

      view.image.resetSelections();

      // Noninverted image
      var c0 = 0;
      var m = 0;
      view.image.selectedChannel = 0;
      var median = view.image.median();
      var avgDev = view.image.avgDev();
      c0 += median + shadowsClipping * avgDev;
      m += median;
      view.image.resetSelections();
      c0 = Math.range(c0, 0.0, 1.0);
      m = this.findMidtonesBalance(targetBackground, m - c0);

	  // If small scale structure only do a soft sretch
	  if(mode == 'SS') {
		  c0 = (c0/2);
		  m  = (m * 1.25);
	  }
      return {m:m, c0:c0, c1:1};
}

function findMidtonesBalance(v0, v1, eps) {
      if (v1 <= 0)
         return 0;

      if (v1 >= 1)
         return 1;

      v0 = Math.range(v0, 0.0, 1.0);

      if (eps)
         eps = Math.max(1.0e-15, eps);
      else
         eps = 5.0e-05;

      var m0, m1;
      if (v1 < v0) {
         m0 = 0;
         m1 = 0.5;
      } else {
         m0 = 0.5;
         m1 = 1;
      }

      for (; ;) {
         var m = (m0 + m1) / 2;
         var v = Math.mtf(m, v1);

         if (Math.abs(v - v0) < eps)
            return m;

         if (v < v0)
            m1 = m;
         else
            m0 = m;
      }
}

// ******************************************
// * Reset To Default						*
// ******************************************
function DialogReset() {
	dialog.SMThreshold.setValue(data.defSMThreshold);
	dialog.SMScale.setValue(data.defSMScale);
	dialog.SMSmoothness.setValue(data.defSMSmoothness);
	dialog.SMSigma.setValue(data.defSMSigma);
	
	dialog.extractLS_CheckBox.checked = data.defViewLS;
	dialog.extractSS_CheckBox.checked = data.defViewSS;
	dialog.extractMS_CheckBox.checked = data.defViewMS;
	dialog.extractFI_CheckBox.checked = data.defViewFI;
	dialog.htLS_CheckBox.checked = data.defHtLS;
	dialog.htSS_CheckBox.checked = data.defHtSS;

	dialog.htLS_CheckBox.enabled 	  = false;
	dialog.htSS_CheckBox.enabled 	  = false;
}

// ******************************************
// * Set To Default For Final Image			*
// ******************************************
function DialogSetFI() {
	if(dialog.extractFI_CheckBox.checked) {
		dialog.extractLS_CheckBox.checked = false;	data.viewLS = false;
		dialog.extractSS_CheckBox.checked = false;	data.viewSS = false;
		dialog.extractMS_CheckBox.checked = false;	data.viewMS = false;
		dialog.htLS_CheckBox.checked 	  = true;	data.htLS   = true;
		dialog.htSS_CheckBox.checked 	  = false;	data.htSS   = false;
		dialog.htLS_CheckBox.enabled 	  = true;
		dialog.htSS_CheckBox.enabled 	  = true;
	} else {
		dialog.extractLS_CheckBox.checked = true;	data.viewLS = true;
		dialog.extractSS_CheckBox.checked = true;	data.viewSS = true;
		dialog.extractMS_CheckBox.checked = false;	data.viewMS = false;
		dialog.htLS_CheckBox.checked 	  = false;	data.htLS   = false;
		dialog.htSS_CheckBox.checked 	  = false;	data.htSS   = false;
		dialog.htLS_CheckBox.enabled 	  = false;
		dialog.htSS_CheckBox.enabled 	  = false;
	}
}

// ******************************************
// * Dialog									*
// ******************************************
function LargeScaleStructureEnhanceDialog() {
   this.__base__ = Dialog;
   this.__base__();


   var emWidth = this.font.width( 'M' );
   var labelWidth = this.font.width( "Convolution Std Dev:" + 'T' );

   // *****************************************
   // Help Text
   // *****************************************
   if(DEFAULT_SHOW_HELP) {
	   this.helpLabel = new Label( this );
	   this.helpLabel.frameStyle = FrameStyle_Box;
	   this.helpLabel.margin = 4;
	   this.helpLabel.wordWrapping = true;
	   this.helpLabel.useRichText = true;
	   this.helpLabel.text =
		  "<p><b>LargeScaleStructureEnhance v" + VERSION + "</b> &mdash; A script for "
		  + " the enhancement of faint large structures such as Integrated Flux Nebulae (IFN) which are very close to the noise floor.</p>"
		  + "<p>Copyright &copy; 2014 Dave Watson. All rights reserved.</p>";
   }
   
   // *** Target Image
   this.targetImage_Label = new Label( this );
   this.targetImage_Label.minWidth = 6; // Align with labels inside group boxes below
   this.targetImage_Label.text = "Target image:";
   this.targetImage_Label.textAlignment = TextAlign_Right|TextAlign_VertCenter;

   this.targetImage_ViewList = new ViewList( this );
   this.targetImage_ViewList.minWidth = 200;
   this.targetImage_ViewList.getAll(); 					// Include main views as well as previews
   this.targetImage_ViewList.currentView = data.targetView;
   this.targetImage_ViewList.toolTip = "Select the image to perform the Dark Structure Enhancement.";
   this.targetImage_ViewList.onViewSelected = function( view ) {
      data.targetView = view;
   };

   this.targetImage_Sizer = new HorizontalSizer;
   this.targetImage_Sizer.spacing = 4;
   this.targetImage_Sizer.add( this.targetImage_Label );
   this.targetImage_Sizer.add( this.targetImage_ViewList, 100 );

   // *****************************************
   // Star Mask Parameters
   // *****************************************
   this.SMThreshold = new NumericControl (this);
   with ( this.SMThreshold ) {
      label.text = "Threshold:";
      label.minWidth = labelWidth;
      setRange (0.0,1.0);
      slider.setRange (0, 1000);
      slider.minWidth = 250;
      setPrecision (1);
      setValue (data.SMThreshold);
      toolTip = "<p>Threshold isolates noise from valid stucture, increasing value will discriminate smaller structure and exclude smaller stars from mask.</p>";
      onValueUpdated = function (value) { data.SMThreshold = value; };
   }
   
   this.SMScale = new NumericControl (this);
   with ( this.SMScale ) {
      label.text = "Scale:";
      label.minWidth = labelWidth
      setRange (8, 10);
      slider.setRange (0, 1000);
      slider.minWidth = 250;
      setPrecision (0);
      setValue (data.SMScale);
      toolTip = "<p>Wavelet layers for structure detection, increase scale to include larger structures.</p>";
      onValueUpdated = function (value) { data.SMScale = value; };
   }
   
   this.SMSmoothness = new NumericControl (this);
   with ( this.SMSmoothness ) {
      label.text = "Smoothness:";
      label.minWidth = labelWidth;
      setRange (5, 15);
      slider.setRange (0, 1000);
      slider.minWidth = 250;
      setPrecision (0);
      setValue (data.SMSmoothness);
      toolTip = "<p>Smoothnes of mask structure, insufficient will lead to edge artifacts, excessive will degrade protection. The default value gives a fair but not overwhelming smoothing of the stars.</p>";
      onValueUpdated = function (value) { data.SMSmoothness = value; };
   }
   
   this.SMSigma = new NumericControl (this);
   with ( this.SMSigma ) {
      label.text = "Convolution Std Dev:";
      label.minWidth = labelWidth;
      setRange (15, 35);
      slider.setRange (0, 1000);
      slider.minWidth = 250;
      setPrecision (0);
      setValue (data.SMSigma);
      toolTip = "<p>The generated mask is always a bit course and convolution is used to smooth the mask. By changing this value the parametric filter will change, in pixels, and the convolution process will act at different dimensional scales.</p>";
      onValueUpdated = function (value) { data.SMSigma = value; };
   }
   
  this.starMaskGroupBox = new GroupBox( this );
   with ( this.starMaskGroupBox )
   {
      title = "Star Mask Parameters";
      sizer = new VerticalSizer;
      sizer.margin = 6;
      sizer.spacing = 4;
      sizer.add(this.SMThreshold);
      sizer.add(this.SMScale);
      sizer.add(this.SMSmoothness);
      sizer.add(this.SMSigma);
   }
   
   
   // *****************************************
   // Extract Images
   // *****************************************
   this.extractLS_CheckBox = new CheckBox( this );
   this.extractLS_CheckBox.text = "Large Scale";
   this.extractLS_CheckBox.checked = data.viewLS;
   this.extractLS_CheckBox.toolTip =
      "<p>If this option is selected, the script will create an image window "
      + "with the Large Scale extracted image.</p>";
   this.extractLS_CheckBox.onCheck = function( checked ) {
      data.viewLS = checked;
   };

   this.extractSS_CheckBox = new CheckBox( this );
   this.extractSS_CheckBox.text = "Small Scale";
   this.extractSS_CheckBox.checked = data.viewSS;
   this.extractSS_CheckBox.toolTip =
      "<p>If this option is selected, the script will create an image window "
      + "with the Small Scale extracted image.</p>";
   this.extractSS_CheckBox.onCheck = function( checked ) {
      data.viewSS = checked;
   };

   this.extractMS_CheckBox = new CheckBox( this );
   this.extractMS_CheckBox.text = "Mask";
   this.extractMS_CheckBox.checked = data.viewMS;
   this.extractMS_CheckBox.toolTip =
      "<p>If this option is selected, the script will create an image window "
      + "with the Star Mask used to extract the Large Scale structure.</p>";
   this.extractMS_CheckBox.onCheck = function( checked ) {
      data.viewMS = checked;
   };

   this.extractFI_CheckBox = new CheckBox( this );
   this.extractFI_CheckBox.text = "Final Image";
   this.extractFI_CheckBox.checked = data.viewFI;
   this.extractFI_CheckBox.toolTip =
      "<p>If this option is selected, the script will create an image window "
      + "with the final image which is the sum of the Large and Small scale images.</p>";
   this.extractFI_CheckBox.onCheck = function( checked ) {
      data.viewFI = checked;
	  DialogSetFI();
   };

   this.extractGroupBox = new GroupBox( this );
   with ( this.extractGroupBox ) {
      title = "Extract Images";
      sizer = new HorizontalSizer;
      sizer.margin = 6;
      sizer.spacing = 4;
      sizer.add(this.extractLS_CheckBox);
      sizer.add(this.extractSS_CheckBox);
      sizer.add(this.extractMS_CheckBox);
      sizer.add(this.extractFI_CheckBox);
   }
 
   this.htSS_CheckBox = new CheckBox( this );
   this.htSS_CheckBox.text = "Small Scale Structure";
   this.htSS_CheckBox.checked = data.htSS;
   this.htSS_CheckBox.toolTip =
      "<p>If this option is selected, the script will perform a soft auto-stretch on "
      + "the Small Scale structure image. This is not recommended as it is a better "
	  + "solution to manually stretch the LS and SS images giving much better control over the final image.</p>";
   this.htSS_CheckBox.onCheck = function( checked ) {
      data.htSS = checked;
   };
   
   this.htLS_CheckBox = new CheckBox( this );
   this.htLS_CheckBox.text = "Large Scale Structure";
   this.htLS_CheckBox.checked = data.htLS;
   this.htLS_CheckBox.toolTip =
      "<p>If this option is selected, the script will perform an auto-stretch on "
      + "the arge Scale structure image. This is not recommended as it is a better "
	  + "solution to manually stretch the LS and SS images giving much better control over the final image.</p>";
   this.htLS_CheckBox.onCheck = function( checked ) {
      data.htLS = checked;
   };

   this.htGroupBox = new GroupBox( this );
   with ( this.htGroupBox ) {
      title = "Auto Histogram Stretch Structure Images";
      sizer = new HorizontalSizer;
      sizer.margin = 6;
      sizer.spacing = 4;
      sizer.add(this.htSS_CheckBox);
      sizer.add(this.htLS_CheckBox);
   }
   
   // *****************************************
   // Control buttons
   // *****************************************
   this.browseDocumentation_Button = new ToolButton(this);
   this.browseDocumentation_Button.icon = ":/process-interface/browse-documentation.png";
   this.browseDocumentation_Button.toolTip =
      "<p>Opens a browser to view the script's documentation</p>";
   this.browseDocumentation_Button.onClick = function () {
      if (!Dialog.browseScriptDocumentation("LargeScaleStructureEnhance")) {
         (new MessageBox(
            "<p>Documentation has not been installed.</p>" +

 			"<p> This script for the enhancement of faint large structures such as Integrated Flux Nebulae (IFN) which are very close to the noise floor. " +
 			" This script is based upon the post-processing workflow as developed by Rogelio Bernal Andreo and the parameter settings are those initially used. " +
			" The script is designed to processes monochrome images that have have been cropped, processed with Dynamic Background Extraction (DBE),  " +
 			" and initially stretched with Histogram Transformation.  " +
 
            "<p>The default settings of the parameters have been determined to give the best results, however, they may need slight adjustment to suit individual images." +
			" Parameters are available to control the star mask and FWHM measurement processes.</p>" +
			
            "<p>Copyright &copy; 2014-2015 Dave Watson. All Rights Reserved.<br/>" + "</p>",

            TITLE + "." + VERSION,
            StdIcon_NoIcon,
            StdButton_Ok
         )).execute();
      }
   };

   this.reset_Button = new ToolButton(this);
   this.reset_Button.icon =  ":/images/icons/reset.png";
   this.reset_Button.toolTip = "<p>Resets the dialog's parameters";
   this.reset_Button.onClick = function() {
     	DialogReset();
   };

    this.ok_Button = new PushButton( this );
    this.ok_Button.icon = ":/icons/power.png";
    this.ok_Button.text = "Run";
    this.ok_Button.onClick = function() {
        this.dialog.ok();
    };

    this.cancel_Button = new PushButton( this );
    this.cancel_Button.icon =  ":/icons/close.png";
    this.cancel_Button.text = "Exit";
    this.cancel_Button.onClick = function() {
        this.dialog.cancel();
    };

    this.buttons_Sizer = new HorizontalSizer;
    this.buttons_Sizer.spacing = 4;
    this.buttons_Sizer.add( this.browseDocumentation_Button );
    this.buttons_Sizer.add( this.reset_Button );
    this.buttons_Sizer.addStretch();
    this.buttons_Sizer.add( this.ok_Button );
    this.buttons_Sizer.add( this.cancel_Button );

   // *****************************************
   // Display Dialogue
   // *****************************************
    this.sizer = new VerticalSizer;
    this.sizer.margin = 8;
    this.sizer.spacing = 6;
	if(DEFAULT_SHOW_HELP) {
    	this.sizer.add( this.helpLabel );
	}
    this.sizer.addSpacing( 4 );
    this.sizer.add( this.targetImage_Sizer );
    this.sizer.add( this.starMaskGroupBox);
    this.sizer.add( this.htGroupBox);
    this.sizer.add( this.extractGroupBox);
    this.sizer.addSpacing( 4 );
    this.sizer.add( this.buttons_Sizer );

    this.windowTitle = TITLE;
    this.adjustToContents();
    this.setFixedSize();

   // *****************************************
   // Disable Final Stretch Checkboxes
   // *****************************************
	this.htLS_CheckBox.enabled = false;
	this.htSS_CheckBox.enabled = false;

	dialog = this;
}

var dialog;
LargeScaleStructureEnhanceDialog.prototype = new Dialog;

// ******************************************
// * Script Entry Point						*
// ******************************************
function main() {
   console.hide();

   if ( !data.targetView ) {
      var msg = new MessageBox( "There is no active image window!",TITLE,StdIcon_Error, StdButton_Ok );
      msg.execute();
      return;
   }

   // We only work with grayscale images
   if (DEFAULT_WARNING_RGB && !data.targetView.image.isGrayscale) {
      (new MessageBox(
         "<p>Target view color space must be Grayscale for correct operation</p>",
         TITLE + "." + VERSION,
         StdIcon_Warning,
         StdButton_Ok
      )).execute();
   }
   
   var dialog = new LargeScaleStructureEnhanceDialog();
   for ( ;; ) {
      if ( !dialog.execute() )
         break;

      // A view must be selected.
      if ( data.targetView.isNull ) {
         var msg = new MessageBox( "You must select a view to apply this script.",TITLE, StdIcon_Error, StdButton_Ok );
         msg.execute();
         continue;
      }

      console.show();
      LargeScaleStructureEnhanceProcess( data );
      break;
   }
}

main();
// ****************************************************************************
// pjsr/LargeScaleStructureEnhance.js
// ****************************************************************************
